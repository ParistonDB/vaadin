package com.example.vaadinek;

import com.example.vaadindemo.domain.Film;
import com.vaadin.annotations.Title;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Title("Vaadin - Kalkulator")
public class VaadinekUI extends UI {

    @Override
    protected void init(VaadinRequest request) {
            Film film = new Film("Wladca Pierscieni", "Film o hobbicie", 2002, "Peter Jackson");
            BeanItem<Film> personItem = new BeanItem<Film>(film);
           
            FormLayout form = new FormLayout();
            FieldGroup binder = new FieldGroup();
           
            binder.setItemDataSource(personItem);
           
            form.addComponent(binder.buildAndBind("Tytul", "tytul"));
            form.addComponent(binder.buildAndBind("Opis", "opis"));
            form.addComponent(binder.buildAndBind("Data", "rokProdukcji"));
            form.addComponent(binder.buildAndBind("Rezyser", "rezyser"));
           
            binder.setBuffered(true);
           
            //binder.getField("lastName").setRequired(true);
            //binder.getField("firstName").setRequired(true);;
           
            VerticalLayout fvl = new VerticalLayout();
            fvl.setMargin(true);;
            fvl.addComponent(form);
           
            setContent(fvl);
    }
}
