package com.example.vaadindemo.domain;

import java.util.Date;
import java.util.UUID;
 
public class Film {
       
        private UUID id;
       
        private String tytul;
        private String opis;
        private String rezyser;
        private int rokProdukcji;
     
        public Film(String tytul, String opis, int rokProdukcji, String rezyser) {
                super();
                this.tytul = tytul;
                this.opis = opis;
                this.rokProdukcji = rokProdukcji;
                this.rezyser = rezyser;
        }
       
        public int getRokProdukcji() {
			return rokProdukcji;
		}

		public void setRokProdukcji(int rokProdukcji) {
			this.rokProdukcji = rokProdukcji;
		}

		public UUID getId() {
                return id;
        }
        public void setId(UUID id) {
                this.id = id;
        }
        public String getTytul() {
                return tytul;
        }
        public void setTytul(String tytul) {
                this.tytul = tytul;
        }
        public String getOpis() {
                return opis;
        }
        public void setOpis(String opis) {
                this.opis = opis;
        }
        public String getRezyser() {
                return rezyser;
        }
        public void setRezyser(String rezyser) {
                this.rezyser = rezyser;
        }
       
       
}
